const edad = document.querySelector('#age');
const boton = document.querySelector('#button');
const licencia = document.querySelector('#licencia');
const nombre = document.querySelector('#nombre');
const apellido = document.querySelector('#apellido');
const fecha = document.querySelector('#fecha');


boton.addEventListener('click', function () {
    const value1 = parseInt(edad.value);
    const name = (nombre.value);
    const surname = (apellido.value);
    const selectedOption = licencia.options[licencia.selectedIndex].value;
    let date = new Date();
	let day = date.getDate();
	let month = date.getMonth() + 1;
	let year = date.getFullYear();


    const fullDate = month < 9 ? year + "0" + month + day : year + month + day;
    if (value1 < 18) {
        alert("Sos menor de edad, no estas habilitado");
    }
    else if (selectedOption === "No") {
        alert("No tenes licencia, no estas habilitado");
    }
    else if (parseInt(fecha.value) < parseInt(fullDate)){
		alert("Su licencia está expirada");
    }    
    else {
        alert(name + " " + surname + " Estas habilitado para conducir.");
    }
    
}
);

